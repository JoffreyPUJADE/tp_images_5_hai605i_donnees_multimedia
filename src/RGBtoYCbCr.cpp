#include <iostream>
#include <stdio.h>
#include <sstream>
#include <string>
#include <cmath>
#include <stdexcept>
#include "Transformations.hpp"

typedef long double ldouble;

enum TypeLuminance
{
	STANDARD,
	PERCEIVED_OPTION_1,
	PERCEIVED_OPTION_2
};

ldouble getRedMult(TypeLuminance tl)
{
	switch(tl)
	{
		case STANDARD :
			return 0.2126;
		break;
		
		case PERCEIVED_OPTION_1 :
		case PERCEIVED_OPTION_2 :
			return 0.299;
		break;
		
		default :
			return -0.1;
		break;
	}
}

ldouble getGreenMult(TypeLuminance tl)
{
	switch(tl)
	{
		case STANDARD :
			return 0.7152;
		break;
		
		case PERCEIVED_OPTION_1 :
		case PERCEIVED_OPTION_2 :
			return 0.587;
		break;
		
		default :
			return -0.1;
		break;
	}
}

ldouble getBlueMult(TypeLuminance tl)
{
	switch(tl)
	{
		case STANDARD :
			return 0.0722;
		break;
		
		case PERCEIVED_OPTION_1 :
		case PERCEIVED_OPTION_2 :
			return 0.114;
		break;
		
		default :
			return -0.1;
		break;
	}
}

ldouble RGBtoYpixel(OCTET red, OCTET green, OCTET blue, TypeLuminance tl)
{
	ldouble redMult = getRedMult(tl);
	ldouble greenMult = getGreenMult(tl);
	ldouble blueMult = getBlueMult(tl);
	
	switch(tl)
	{
		case STANDARD :
		case PERCEIVED_OPTION_1 :
			return ((red * redMult) + (green * greenMult) + (blue * blueMult));
		break;
		
		case PERCEIVED_OPTION_2 :
			return sqrt(((red * red) * redMult) + ((green * green) * greenMult) + ((blue * blue) * blueMult));
		break;
		
		default :
			return -0.1;
		break;
	}
}

ldouble RGBtoCbpixel(OCTET red, OCTET green, OCTET blue)
{
	return ((-0.1687 * red) - (0.3313 * green) + (0.5 * blue) + 128.0);
}

ldouble RGBtoCrpixel(OCTET red, OCTET green, OCTET blue)
{
	return ((0.5 * red) - (0.4187 * green) + (0.0813 * blue) + 128.0);
}

void RGBtoYCbCr(OCTET red, OCTET green, OCTET blue, ldouble& Y, ldouble& Cb, ldouble& Cr)
{
	Y = RGBtoYpixel(red, green, blue, PERCEIVED_OPTION_1);
	Cb = RGBtoCbpixel(red, green, blue);
	Cr = RGBtoCrpixel(red, green, blue);
}

int main(int argc, char** argv)
{
	int nH, nW, nTaille;

	if(argc != 2)
		throw std::runtime_error("Usage: ImageIn.ppm");
	
	std::string nomImageLue = argv[1];
	
	OCTET *ImgIn, *ImgOutY, *ImgOutCb, *ImgOutCr;
	
	lire_nb_lignes_colonnes_image_ppm(nomImageLue.c_str(), &nH, &nW);
	
	nTaille = nH * nW;
	
	int nTaille3 = (nTaille * 3);
	
	allocation_tableau(ImgIn, OCTET, nTaille3);
	lire_image_ppm(nomImageLue.c_str(), ImgIn, nH * nW);
	allocation_tableau(ImgOut, OCTET, nTaille);
	
	OCTET *tabRouge, *tabVert, *tabBleu;
	
	allocation_tableau(tabRouge, OCTET, nTaille);
	allocation_tableau(tabVert, OCTET, nTaille);
	allocation_tableau(tabBleu, OCTET, nTaille);
	
	planR(tabRouge, ImgIn, nTaille);
	planV(tabVert, ImgIn, nTaille);
	planB(tabBleu, ImgIn, nTaille);
	
	for(int i=0;i<nTaille;++i)
	{
		ldouble Y;
		ldouble Cb;
		ldouble Cr;
		
		RGBtoYCbCr(tabRouge[i], tabVert[i], tabBleu[i], Y, Cb, Cr);
		
		ImgOut[i] = Y;
		ImgOut[i] = Cb;
		ImgOut[3*i+2] = Cr;
	}
	
	std::string nomImageEcrite = nomImageLue.substr(0, nomImageLue.find_last_of("."));
	
	ecrire_image_pgm((nomImageEcrite + "_Y.pgm").c_str(), ImgOut, nH, nW);
	ecrire_image_pgm((nomImageEcrite + "_Cb.pgm").c_str(), ImgOut, nH, nW);
	ecrire_image_pgm((nomImageEcrite + "_Cr.pgm").c_str(), ImgOut, nH, nW);
	
	free(ImgIn);
	free(ImgOut);
	
	return 0;
}
