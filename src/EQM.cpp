#include <iostream>
#include <stdio.h>
#include <sstream>
#include <string>
#include <cmath>
#include <stdexcept>
#include "Transformations.hpp"

typedef long double ldouble;

int main(int argc, char** argv)
{
	int nH, nW, nTaille;

	if(argc != 3)
		throw std::runtime_error("Usage: ImageInOriginale.pgm ImageInProgramme.pgm");
	
	std::string nomImageLueOriginale = argv[1];
	std::string nomImageLueProgramme = argv[2];
	
	OCTET *ImgInOriginale, *ImgInProgramme;
	
	lire_nb_lignes_colonnes_image_pgm(nomImageLueOriginale.c_str(), &nH, &nW);
	
	nTaille = nH * nW;
	
	allocation_tableau(ImgInOriginale, OCTET, nTaille);
	lire_image_pgm(nomImageLueOriginale.c_str(), ImgInOriginale, nH * nW);
	allocation_tableau(ImgInProgramme, OCTET, nTaille);
	lire_image_pgm(nomImageLueProgramme.c_str(), ImgInProgramme, nH * nW);
	
	ldouble eqm = 0.0;
	
	for(int i=0;i<nTaille;++i)
		eqm += pow((ImgInOriginale[i] - ImgInProgramme[i]), 2);
	
	free(ImgInOriginale);
	free(ImgInProgramme);
	
	eqm /= nTaille;
	
	std::cout << "Erreur Quadratique Moyenne : " << eqm << std::endl;
	
	return 0;
}
